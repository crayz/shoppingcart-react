from flask import Flask, session, redirect
from flask import g
from datetime import timedelta
from modules import default
from modules import users
from modules import inventory

from pymongo import MongoClient

app = Flask(__name__)

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.postsdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.itemsdb = inventory.inventoryDB(conn=mainDb.items)
    g.cartdb = inventory.cartDB(conn=mainDb.carts)
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=120)
    session.modified = True
    
    
@app.teardown_request
def teardown_request(exception):
    postdb = getattr(g, 'postdb', None)
    if postdb is not None:
        postdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()

app.register_blueprint(default.mod)