class inventoryDB:
    def __init__(self, conn):
        self.conn = conn

    def getItems(self):
        return self.conn.find()
     
    def addItem(self, item_name, price):
        self.conn.insert({'item_name':item_name, 'price': price})