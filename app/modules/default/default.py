from datetime import datetime
from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import request
from flask import g
from flask import flash
from flask import json
from ..forms import reg_form, add_form
from ..models import user as UserRef
from ..inventory.inventory_db import inventoryDB
from functools import wraps
from bson.objectid import ObjectId
from bson import json_util
from bson.json_util import dumps

mod = Blueprint('default', __name__)

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect('/login')
        else:
            return func(*args, **kwargs)
    return wrapper 

@mod.route('/')
def index():
    if 'username' not in session:
        return redirect('/login')
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return render_template('default/index.html', server_time=current_time)

@mod.route('/login', methods=['GET'])
def login_page():
    if 'username' in session:
        return redirect('/')
    return render_template('default/login.html')

@mod.route('/login', methods=['POST'])
def login_submit():
    username = request.form['username']
    password = request.form['password']
    if g.usersdb.getUserWithPassword(username, password).count() > 0:
        session['username'] = username
        return redirect('/')
    else:
        flash('Invalid username and password.', 'signin_failure')
        return redirect('/login') 

@mod.route('/logout', methods=['GET'])
def logout_submit():
    session.pop('username', None)
    session.clear()
    return redirect('/login')

@mod.route('/register', methods=['GET', 'POST'])
def register():
    form = reg_form.RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = UserRef.User(form.username.data, form.email.data,
                    form.password.data)
        g.usersdb.createUserUsingUser(user)
        session['username'] = form.username.data
        return redirect('/')
    return render_template('default/signup.html', form=form)

@mod.route('/items', methods=['GET', 'POST'])
def inventory():
    form = add_form.AddForm(request.form)
    if request.method == 'POST':
        if form.validate():
            g.itemsdb.addItem(form.item_name.data, form.price.data)
            flash('New Item Added!', 'add_item_success')
            items = g.itemsdb.getItems()
            count = 0
            newItems=[]
            for item in items:
                rec = dumps(item,default=json_util.default)
                json1_data = json.loads(rec)
                objectid = json1_data['_id']
                oidstring = objectid['$oid']
                x = '{item_name:"' + json1_data["item_name"] + '",price:"' + json1_data["price"] + '",OID:"oidstring"}'
                newItems.insert(count,x)
                count += 1  
            return render_template('default/inventory.html', items=newItems)
        else:
            for fieldName in form.errors:
                for err in form.errors[fieldName]:
                    flash(err, 'add_fail')
    items = g.itemsdb.getItems()
    count = 0
    newItems=[]
    for item in items:
        rec = dumps(item,default=json_util.default)
        json1_data = json.loads(rec)
        objectid = json1_data['_id']
        oidstring = objectid['$oid']
        x = '{item_name:"' + json1_data["item_name"] + '",price:"' + json1_data["price"] + '",OID:"oidstring"}'
        newItems.insert(count,x)
        count += 1  
    return render_template('default/inventory.html', items=newItems)

@mod.route('/cart', methods=['GET', 'POST'])
def cart():
    form_data = request.form
    if request.method == 'POST':
        check = g.cartdb.checkCart(request.form['item'], request.form['price'], session['username'])
        if check:
            objid = check['_id']
            new_quant = int(check['quantity']) + 1
            g.cartdb.addCart(form_data['item'], form_data['price'], session['username'], new_quant, 1, objid)
            flash(form_data['item'] + ' Added to cart!', 'add_to_cart_success')
        else:
            g.cartdb.addCart(form_data['item'], form_data['price'], session['username'], '1')
            flash(form_data['item'] + ' Added to cart!', 'add_to_cart_success')
    items = g.cartdb.getCart(session['username'])
    print "\n\n\n\n"
    print items[0]
    print "\n\n\n"
    count = 0
    newItems=[]
    for item in items:
        rec = dumps(item,default=json_util.default)
        json1_data = json.loads(rec)
        objectid = json1_data['_id']
        oidstring = objectid['$oid']
        x = '{item_name:"' + str(json1_data['item_name']) + '",price:"' + str(json1_data['price']) + '",item_total:"' + str(json1_data['price']) + '",OID:"' + str(oidstring) + '",quantity:"' + str(json1_data['quantity']) + '"}'
        newItems.insert(count,x)
        count += 1  
    x = 0
    i = g.cartdb.getCart(session['username'])
    for item in i:
        x += item['item_total']
    return render_template('default/cart.html', items=newItems, gtotal=x)



